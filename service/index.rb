
require 'sinatra'

set :port => 80

get '/' do
  'Hello world!'
end

get '/pi' do
  { :isPi => true, :really => true }.to_json
end